package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/xanzy/go-gitlab"
)

const (
	tagMessage = "Auto-generated. Do not Remove."
)

var (
	requiredEnvs = []string{
		"CI_API_V4_URL",
		"GITLAB_TOKEN",
		"CI_COMMIT_SHORT_SHA",
		"CI_COMMIT_SHA",
		"CI_PROJECT_PATH",
	}
	httpCli = &http.Client{
		Timeout: time.Second * 10,
		Transport: &http.Transport{
			Dial: (&net.Dialer{
				Timeout: 5 * time.Second,
			}).Dial,
			TLSHandshakeTimeout: 5 * time.Second,
		},
	}
	tagFlag     = flag.String("tag", "", "Use this user-defined tag.")
	rewriteFlag = flag.Bool("rewrite", false, "Rewrite existing tag.")
)

func ValidateEnvironment() []error {
	errs := []error{}
	for _, env := range requiredEnvs {
		if len(os.Getenv(env)) == 0 {
			errs = append(errs, fmt.Errorf("Variable %s must be specified", env))
		}
	}
	return errs
}

func main() {
	flag.Parse()
	errs := ValidateEnvironment()
	if len(errs) > 0 {
		for _, err := range errs {
			log.Print(err)
		}
		os.Exit(1)
	}
	cli := gitlab.NewClient(httpCli, os.Getenv("GITLAB_TOKEN"))
	err := cli.SetBaseURL(os.Getenv("CI_API_V4_URL"))
	if err != nil {
		log.Fatal(err)
	}
	tagName := fmt.Sprintf(
		"v%s-%s",
		time.Now().Format("20060102"),
		os.Getenv("CI_COMMIT_SHORT_SHA"),
	)
	if len(*tagFlag) > 0 {
		tagName = *tagFlag
	}
	opts := &gitlab.CreateTagOptions{
		TagName: gitlab.String(tagName),
		Ref:     gitlab.String(os.Getenv("CI_COMMIT_SHA")),
		Message: gitlab.String(tagMessage),
	}
	if *rewriteFlag {
		_, err := cli.Tags.DeleteTag(
			os.Getenv("CI_PROJECT_PATH"),
			tagName,
			nil,
		)
		if err != nil && !strings.Contains(err.Error(), "Tag Not Found") {
			log.Println(err)
		}
	}
	tag, _, err := cli.Tags.CreateTag(
		os.Getenv("CI_PROJECT_PATH"),
		opts,
		nil,
	)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Tagged as %s", tag.Name)
}
